package lk.company.price_engine;

import lk.company.price_engine.controller.PriceController;
import lk.company.price_engine.controller.ProductController;
import lk.company.price_engine.dto.ProductDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class WebAppicationTest {


    @Autowired(required = false)
    private ProductController productController;

    @Autowired(required = false)
    private PriceController priceController;

    @Test
    public void productControllerLoad() {
        assertThat(productController).isNotNull();
    }

    @Test
    public void priceControllerLoad() {
        assertThat(priceController).isNotNull();
    }

    @Test
    public void getAllProductsWithVariations() {
        assertThat(productController.findAllProducts(50))
                .isInstanceOf(List.class);
    }
    @Test
    public void getAllProductsWithOutVariations() {
        assertThat(productController.findAllProducts(0))
                .isInstanceOf(List.class);
    }
}
